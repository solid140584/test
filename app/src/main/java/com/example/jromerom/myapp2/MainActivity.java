package com.example.jromerom.myapp2;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RelativeLayout;

public class MainActivity extends AppCompatActivity {

    private RelativeLayout rlPrincipal;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        rlPrincipal = (RelativeLayout)findViewById(R.id.rlPrincipal);

        Button buyButton = new Button(this);
        buyButton.setText("Boton de prueba");
        buyButton.setLayoutParams(new RelativeLayout.LayoutParams(
                ViewGroup.LayoutParams.WRAP_CONTENT,
                ViewGroup.LayoutParams.WRAP_CONTENT));
       // rlPrincipal.addView(GT); // line 27
        rlPrincipal.addView(buyButton);
        setContentView(rlPrincipal);
    }
}
